﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XlsxAgent.Extensions
{
    internal static class StringbuilderExstensions
    {
        public static void AppendT(this StringBuilder sb, string message)
        {
            sb.AppendLine($"{DateTime.Now:T}:  {message}");
        }
    }
}