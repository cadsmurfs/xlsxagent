﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace XlsxAgent.Extensions
{
    internal static class ExcelExstensions
    {
        #region Public Methods

        public static List<string> GetListOfFirstRow(this ExcelRange range)
        {
            /*
             Some info: https://github.com/JanKallman/EPPlus/issues/259
             Solution was here: https://www.c-sharpcorner.com/forums/eeplus-how-to-get-range-and-loop
             */

            var start = range.Start;
            var end = range.End;
            var result = new List<string>();

            // First Row
            for (int col = start.Column; col <= end.Column; col++)
            {
                // ... Cell by cell...
                string cellValue = range[start.Row, col].Text; // This got me the actual value I needed.
                // object cellValue2 = range[start.Row, col].Value;
                result.Add(cellValue);
            }

            return result;
        }

        /// <summary>
        /// careful: this function is only valid in case of strings starting with
        /// letters, followed by number think excel style. ea A11; FF15; ...
        /// </summary>
        /// <param name="input">  </param>
        /// <param name="value">  </param>
        /// <returns>  </returns>
        internal static string IncrementNumber(this string input, int value = 1)
        {
            // careful: this function is only valid in case of strings starting
            // with letters, followed by number think excel style.

            // Split on one or more non-digit characters.
            string numb = Regex.Replace(input, @"\D+", string.Empty);
            string alpha = Regex.Replace(input, @"[\d-]", string.Empty);
            int.TryParse(numb, out int intnumb);
            return alpha + (intnumb + value).ToString();
        }

        internal static int GetExcelColumnNumber(this string colName)
        {
            int col = colName.ToCharArray()
                .Select(c => c - 'A' + 1)
                .Reverse()
                .Select((v, i) => v * (int)Math.Pow(26, i)).Sum();
            return col;
        }

        internal static string GetExcelName(this int i)
        {
            int div = i;
            string colLetter = string.Empty;
            while (div > 0)
            {
                int mod = (div - 1) % 26;
                colLetter = (char)(65 + mod) + colLetter;
                div = (div - mod) / 26;
            }
            return colLetter;
        }

        #endregion Public Methods
    }
}