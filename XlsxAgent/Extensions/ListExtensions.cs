﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XlsxAgent.Extensions
{
    internal static class ListExtensions
    {
        /// <summary>
        /// Returns a new list of string, with all empty strings removed from
        /// the beginning and end of the string
        /// </summary>
        /// <param name="list"> </param>
        /// <returns> </returns>
        public static List<string> Trim(this IList<string> list)
        {
            int start = 0, end = list.Count - 1;

            while (start < end && string.IsNullOrWhiteSpace(list[start])) start++;
            while (end >= start && string.IsNullOrWhiteSpace(list[end])) end--;

            return list.Skip(start).Take(end - start + 1).ToList();
        }

        /// <summary>
        /// Returns a new list of string, with all empty strings removed from
        /// the end of the string
        /// </summary>
        /// <param name="list"> </param>
        /// <returns> </returns>
        public static List<string> TrimEnd(this IList<string> list)
        {
            const int start = 0;
            var end = list.Count - 1;

            // while (start < end && string.IsNullOrWhiteSpace(list[start])) start++;
            while (end >= start && string.IsNullOrWhiteSpace(list[end])) end--;

            return list.Skip(start).Take(end - start + 1).ToList();
        }
    }
}