﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using XlsxAgent.Extensions;
using XlsxAgent.Model;

namespace XlsxAgent.Data.ReaderWriters
{
    public class TUCReaderWriter : BaseReaderWriter
    {
        #region Public Constructors

        public TUCReaderWriter()
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExpectedSignature = new string[,] {
                { "A1", "Cadre nr." },
                { "B1", "Service" },
                { "C1" ,"Tension" },
                { "D1", "Cable Type" },
                { "E1", "Cabeltrack from/to" },
                { "F1", "Cable nr." },
                { "G1" ,"Line nr." },
                { "H1", "Placement" },
                { "I1", "Remarks" },
                { "J1", "EDX" },
                { "K1", "Depth (cm)" },
                { "L1", "Length (m)" },
                { "M1", "Temp1" },
                { "N1", "Temp2" },
                { "O1", "Temp3" }
            };
        }

        #endregion Public Constructors

        #region Public Methods

        /// <summary>
        /// </summary>
        /// <exception cref="NullReferenceException">  </exception>
        /// <returns> False when header is not found on file. </returns>
        public override bool Read()
        {
            CheckIfFileExists();

            using (var package = new ExcelPackage(File))
            {
                var worksheet = package.Workbook.Worksheets.First();

                //check if the worksheet is completely empty
                if (worksheet.Dimension == null)
                {
                    throw new NullReferenceException("The first worksheet in the provided excel file is empty.");
                }

                var allCells = worksheet.Cells;

                // check if our TUC specific header is in this file
                ExpectedHeaderResult searchResult = SearchForHeaderSignature(worksheet);

                Debug.Assert(searchResult.ResultType == ExpectedSignatureResultTypeEnum.FullMatch);

                // OK, lets process this file row by row
                var start = worksheet.Dimension.Start;
                var end = worksheet.Dimension.End;

                Frame lastFrame = null;

                for (int row = searchResult.RowOffset + start.Row; row <= end.Row; row++)
                { // Row by row...

                    string service, tension, cableType, cableTrackFromTo, cableNr, lineNumber, remarks, edx, depth, length, temp1, temp2, temp3;
                    service = tension = cableType = cableTrackFromTo = cableNr = lineNumber = remarks = edx = depth = length = temp1 = temp2 = temp3 = "";
                    for (int col = start.Column; col <= end.Column; col++)
                    { // ... Cell by cell...
                        // string cellValue = (worksheet.Cells[row, col].Value ==
                        // null) ? "" : worksheet.Cells[row, col].Value.ToString();
                        TryGetCellValue(out string cellValue, col.GetExcelName() + row, worksheet);

                        // if it is a header, and we are at first column, we can
                        // skip to next row
                        if (col == start.Column &&
                            cellValue.Equals(ExpectedSignature[0, 1],
                            StringComparison.CurrentCultureIgnoreCase))
                            break;

                        // if we are at first column, and the first value is an
                        // int we are at the beginning of a frame.
                        if (col == start.Column && int.TryParse(cellValue, out int nr))
                        {
                            lastFrame = new Frame { Number = nr, Type = "Unassigned" };
                            Frames.Add(lastFrame);
                            continue;
                        }
                        // If we are at start column, and there is no value, we
                        // can continue to next column
                        if (col == start.Column && string.IsNullOrEmpty(cellValue))
                            continue;

                        if (col == start.Column + 1)
                        { service = cellValue; continue; }
                        if (col == start.Column + 2)
                        { tension = cellValue; continue; }
                        if (col == start.Column + 3)
                        { cableType = cellValue; continue; }
                        if (col == start.Column + 4)
                        { cableTrackFromTo = cellValue; continue; }
                        if (col == start.Column + 5)
                        { cableNr = cellValue; continue; }
                        if (col == start.Column + 6)
                        { lineNumber = cellValue; continue; }
                        if (col == start.Column + 7)
                        {
                            // here we can set type of our current frame
                            if (lastFrame.Type != "Unassigned")
                                // its already set, don't do anything
                                continue;

                            lastFrame.Type = cellValue;
                            continue;
                        }

                        if (col == start.Column + 8)
                        { remarks = cellValue; continue; }

                        if (col == start.Column + 9)
                        { edx = cellValue; continue; }

                        if (col == start.Column + 10)
                        { depth = cellValue; continue; }

                        if (col == start.Column + 11)
                        { length = cellValue; continue; }

                        if (col == start.Column + 12)
                        { temp1 = cellValue; continue; }

                        if (col == start.Column + 13)
                        { temp2 = cellValue; continue; }

                        if (col != start.Column + 14) continue;
                        temp3 = cellValue;

                    }

                    // check for empty lines, and skip them
                    var key = Cable.GetKeyName(service, cableType, cableTrackFromTo, cableNr);
                    if (string.IsNullOrWhiteSpace(key))
                        continue;

                    // If cable does not yet exist in cable list, and cable from
                    // to is not an empty string, add it
                    if (!string.IsNullOrWhiteSpace(cableTrackFromTo) &&
                        !CableSet.Exists(x => x.KeyName.Equals(key)))
                    {
                        var newCable = new Cable(cableTrackFromTo, new Service(service), cableType, cableNr)
                        {
                            Frames = new List<Guid> { lastFrame.Id },
                            Tension = tension,
                            CableNr = cableNr,
                            LineNumber = lineNumber,
                            Remarks = remarks,
                            EDX = edx,
                            Depth = depth,
                            Length = length,
                            Temp1 = temp1,
                            Temp2 = temp2,
                            Temp3 = temp3,
                        };
                        CableSet.Add(newCable);
                    }
                    else
                    {
                        var cb = CableSet.Find(x => x.KeyName.Equals(key));
                        if (cb != null && cb.Frames != null && !cb.Frames.Contains(lastFrame.Id))
                        {
                            cb.Frames.Add(lastFrame.Id);
                        }
                    }
                }
            }

            // make sure all frames that do not have a type (value of type is an empty
            // string), are assigned the string "Unassigned". We need this when
            // converting back to a matrix.
            Frames.FindAll(frame => string.IsNullOrWhiteSpace(frame.Type)).ForEach(frame => frame.Type = "Unassigned");

            return true;
        }

        public override void Write()
        {
            // This is the header we add to each matrix frame
            string[] header = { "Cadre nr.", "Service", "Tension", "Cable Type", "Cabeltrack from/to", "Cable nr.", "Line nr.", "Placement", "Remarks",
                   "EDX", "Depth (cm)", "Length (m)", "Temp1", "Temp2", "Temp3" };

            // Add our sheet to new Excel, if it already exist we append it.
            using (var package = new ExcelPackage(File))
            {
                var worksheet = package.Workbook.Worksheets.Add(Path.GetFileNameWithoutExtension(File.FullName));

                var headerStyle = package.Workbook.Styles.CreateNamedStyle("HeaderStyle");
                headerStyle.Style.Fill.PatternType = ExcelFillStyle.Solid;
                headerStyle.Style.Fill.BackgroundColor.Indexed = 22;
                headerStyle.Style.Font.Bold = true;

                var leftSideStyle = package.Workbook.Styles.CreateNamedStyle("LeftSideStyle");
                leftSideStyle.Style.Fill.PatternType = ExcelFillStyle.Solid;
                leftSideStyle.Style.Fill.BackgroundColor.Indexed = 40;
                leftSideStyle.Style.Font.Bold = true;

                // Sort frames
                var sortedFrames = Frames.OrderBy(x => x.Number).ToList();

                // we start at the top of page left corner.
                int row = 1;
                // For each frame
                foreach (var currentFrame in sortedFrames)

                {
                    var collumn = 1;

                    //Create Header
                    for (int i = 0; i < header.Length; i++)
                    {
                        worksheet.Cells[row, collumn].StyleName = "HeaderStyle";
                        worksheet.Cells[row, collumn++].Value = header[i];
                    }

                    // reset columns
                    collumn = 1;

                    // First row set, so up it
                    row++;

                    // Now for each cable add a row, frame number is only added
                    // to the first cable
                    bool firstPassed = false;

                    // Get all cables that are member of the frame, sort them by
                    // name, grouped by sorted service
                    foreach (var group in SortCables(CableSet.FindAll(cb => cb.Frames.Contains(currentFrame.Id))))
                        foreach (var cable in group)
                        {
                            string[] cableCells = new string[header.Length];
                            for (int i = 0; i < header.Length; i++)
                            {
                                if (!firstPassed)
                                {
                                    cableCells[0] = currentFrame.Number.ToString();
                                }
                                else cableCells[0] = "";
                                cableCells[1] = cable.Service.Name;
                                cableCells[2] = cable.Tension;
                                cableCells[3] = cable.CableType;
                                cableCells[4] = cable.CableTrackFromTo;
                                cableCells[5] = cable.CableNr;
                                cableCells[6] = cable.LineNumber;
                                cableCells[7] = currentFrame.Type;
                                cableCells[8] = cable.Remarks;
                                cableCells[9] = cable.EDX;
                                cableCells[10] = cable.Depth;
                                cableCells[11] = cable.Length;
                                cableCells[12] = cable.Temp1;
                                cableCells[13] = cable.Temp2;
                                cableCells[14] = cable.Temp3;
                            }

                            //Create CABLE
                            for (int i = 0; i < cableCells.Length; i++)
                            {
                                if (i == 0)
                                    worksheet.Cells[row, collumn].StyleName = "LeftSideStyle";
                                worksheet.Cells[row, collumn++].Value = string.IsNullOrWhiteSpace(cableCells[i]) ? null : cableCells[i];
                            }
                            // reset columns
                            collumn = 1;

                            // Up row
                            row++;
                            firstPassed = true;
                        }

                    // Add empty row with frame number when we have no cables
                    if (!SortCables(CableSet.FindAll(cb => cb.Frames.Contains(currentFrame.Id))).Any())
                    {
                        for (int i = 0; i < header.Length; i++)
                        {
                            if (i == 0)
                            {
                                worksheet.Cells[row, collumn].Value = currentFrame.Number.ToString();
                                worksheet.Cells[row, collumn++].StyleName = "LeftSideStyle";
                            }
                            else
                                worksheet.Cells[row, collumn++].Value = "";
                        }

                        // reset columns
                        collumn = 1;
                        // Up row
                        row++;
                    }

                    // reset columns
                    collumn = 1;
                    // Up row
                    row++;

                    // Add empty row unless this is the last FRAME
                    if (currentFrame.Equals(sortedFrames.Last()))
                        for (int i = 0; i < header.Length; i++)
                            worksheet.Cells[row, collumn++].Value = "";
                }

                worksheet.Cells.AutoFitColumns();
                package.Save();
            }
        }

        #endregion Public Methods
    }
}