﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using XlsxAgent.Extensions;
using XlsxAgent.Model;

namespace XlsxAgent.Data.ReaderWriters
{
    public abstract class BaseReaderWriter
    {
        #region Public Fields

        public List<Cable> CableSet = new List<Cable>();
        public List<Frame> Frames = new List<Frame>();

        #endregion Public Fields

        #region Public Properties

        protected string[,] ExpectedSignature { get; set; }
        public FileInfo File { get; set; }

        #endregion Public Properties

        #region Public Methods

        abstract public bool Read();

        abstract public void Write();

        #endregion Public Methods

        #region Internal Methods

        /// <summary>
        /// Throws file not found exception whenever the file does not exist
        /// </summary>
        /// <exception cref="FileNotFoundException"> File not found. </exception>
        internal void CheckIfFileExists()
        {
            if (File == null) throw new InvalidDataException("U should set the File property of the reader object first before calling read.");

            if (File.Exists)
                return;
            else
                throw new FileNotFoundException($"File not found: {File.FullName}");
        }

        internal class ExpectedHeaderResult
        {
            public ExpectedSignatureResultTypeEnum ResultType
            {
                get
                {
                    if (PaternMatches == null)
                        return ExpectedSignatureResultTypeEnum.NotStarted;
                    else
                    {
                        var count = PaternMatches.Count(x => x.Match);
                        if (count == 0) return ExpectedSignatureResultTypeEnum.NoMatch;
                        else if (count == PaternMatches.Count) return ExpectedSignatureResultTypeEnum.FullMatch;
                        else return ExpectedSignatureResultTypeEnum.PartialMatch;
                    }
                }
            }

            public int RowOffset { get; set; }
            public List<PatternMatchResult> PaternMatches { get; set; }
        }

        internal enum ExpectedSignatureResultTypeEnum
        {
            NotStarted, NoMatch, FullMatch, PartialMatch
        }

        internal struct PatternMatchResult
        {
            public string ExpectedValue { get; set; }
            public string ActualValue { get; set; }
            public string Adress { get; set; }
            public bool Match { get; set; }

            public string Report { get { return $"Matched {Match} at address ({Adress}), expected ({ExpectedValue}), got ({ActualValue})."; } }
        }

        internal ExpectedHeaderResult FindSignatureHeader(ExcelWorksheet worksheet, int maxSearchDepth = 30)
        {
            if (worksheet is null)
                throw new ArgumentNullException(nameof(worksheet));

            var searchResult = new ExpectedHeaderResult();
            var upperLimitRowsInsheet = worksheet.Dimension.Rows > maxSearchDepth
                ? maxSearchDepth
                : worksheet.Dimension.Rows;

            // for every row in our file
            for (int i_rows = 0; i_rows < upperLimitRowsInsheet; i_rows++)
            {
                // for every value pair of the expected signature, we will look
                // for an equality. All members need to match before the
                // signature is found. When we fail to find a match, we should
                // report where we failed, and what we expected. If we hit the
                // end of the file, or maxSearchDept, we should report signature
                // was not found.
                searchResult.PaternMatches = new List<PatternMatchResult>();
                for (int j = 0; j < ExpectedSignature.GetLength(0); j++)
                {
                    var paternMatch = new PatternMatchResult
                    {
                        Adress = ExpectedSignature[j, 0].IncrementNumber(i_rows),
                        ExpectedValue = ExpectedSignature[j, 1],
                        Match = false
                    };
                    searchResult.RowOffset = i_rows;

                    if (!TryGetCellValue(out string textValue, paternMatch.Adress, worksheet))
                    {
                        paternMatch.Match = false;
                        paternMatch.ActualValue = textValue;
                    }
                    else
                    {
                        if (textValue.Equals(paternMatch.ExpectedValue))
                            paternMatch.Match = true;
                        paternMatch.ActualValue = textValue;
                    }

                    // add our match to our list
                    searchResult.PaternMatches.Add(paternMatch);
                }
                if (searchResult.ResultType == ExpectedSignatureResultTypeEnum.FullMatch || searchResult.ResultType == ExpectedSignatureResultTypeEnum.PartialMatch)
                    return searchResult;
            }
            // we searched all rows, but did not find a full match
            return searchResult;
        }

        internal ExpectedHeaderResult SearchForHeaderSignature(ExcelWorksheet worksheet)
        {
            var searchResult = FindSignatureHeader(worksheet);

            var searchReport = string.Join("; ", searchResult.PaternMatches.FindAll(x => !x.Match).Select(x => x.Report).ToArray());

            if (searchResult.ResultType == ExpectedSignatureResultTypeEnum.NoMatch) throw new InvalidDataException($"We failed to find the expected header/signature.");
            if (searchResult.ResultType == ExpectedSignatureResultTypeEnum.PartialMatch) throw new InvalidDataException($"We could only partially match the signature: \r\n ·  ·  ·   ├─>{searchReport}");
            return searchResult;
        }

        internal IEnumerable<IList<Cable>> SortCables(List<Cable> inputList)
        {
            return inputList
                    .GroupBy(cable => cable.Service.SortWeight)
                    .OrderBy(group => group.Key)
                    .Select(group => group.OrderBy(x => x.CableTrackFromTo, new Compares.AlphaNumericComparer()).ToList());
        }

        internal bool TryGetCellValue(out string value, string adress, ExcelWorksheet worksheet)
        {
            value = "";
            object obj = worksheet.Cells[adress].Value;
            if (obj != null)
            {
                value = obj.ToString();
                return true;
            }
            return false;
        }

        #endregion Internal Methods
    }
}