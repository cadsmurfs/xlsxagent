﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using XlsxAgent.Extensions;
using XlsxAgent.Model;

namespace XlsxAgent.Data.ReaderWriters
{
    public class InfrabelReaderWriter : BaseReaderWriter
    {
        #region Public Constructors

        public InfrabelReaderWriter()
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExpectedSignature = new string[,] {
                { "A3", "Service" },
                { "B3", "Tension" },
                { "C3" ,"Cable Type" },
                { "D3", "Cabeltrack from/to" },
                { "E3", "Cable nr." },
                { "F3" ,"Line nr." },
                { "G3", "Remarks" },
                { "H3", "EDX" },
                { "I3", "Depth (cm)" },
                { "J3", "Length (m)" },
                { "K3", "Temp1" },
                { "L3", "Temp2" },
                { "M3", "Temp3" },
                { "N3", "Placement" } };
        }

        #endregion

        #region Public Properties

        public int ExtraColumns { get; set; } = 50;
        public int ExtraRows { get; set; } = 50;

        #endregion

        #region Public Methods

        /// <summary>
        /// </summary>
        /// <exception cref="InvalidDataException">
        /// Throws an InvalidDataException whenever a matrix is found, and the
        /// value for the Matrix Number can not be converted into an integer.
        /// </exception>
        /// <exception cref="NullReferenceException">
        /// The first worksheet in the provided excel file is empty.
        /// </exception>
        /// <returns>
        /// <see langword="false" /> when expected header is not found in file.
        /// </returns>
        public override bool Read()
        {
            CheckIfFileExists();

            using (var package = new ExcelPackage(File))
            {
                var worksheet = package.Workbook.Worksheets.First();

                //check if the worksheet is completely empty
                if (worksheet.Dimension == null)
                {
                    throw new NullReferenceException("The first worksheet in the provided excel file is empty.");
                }

                var allCells = worksheet.Cells;

                // check if our Infrabel specific header is in this file
                ExpectedHeaderResult searchResult = SearchForHeaderSignature(worksheet);

                // Now that we have the expected data format, iterate the
                // worksheet row by row, and create our data-set
                var maxRows = worksheet.Dimension.Rows + 1;
                var maxColumns = worksheet.Dimension.Columns;

                // how much frames have been specified? frames start at O(4+offset)
                var addressStart = "N4".IncrementNumber(searchResult.RowOffset);
                var address = $"{addressStart}:{maxColumns.GetExcelName()}{4 + searchResult.RowOffset}";

                // create the frames (edit issue#11 remove trailing empty[frames
                // with no number assigned] frames, we added before). We do not
                // remove empty frames from beginning or in the middle of the
                // array. They should still throw InvalidOperationException.
                int? lastCall = null;
                allCells[address].GetListOfFirstRow().TrimEnd().ForEach(x =>
                {
                    var lastCallMsg = lastCall.HasValue ? $"The frame that comes after frame number [{lastCall.Value}]" : "first element in list";

                    if (string.IsNullOrWhiteSpace(x))
                        throw new InvalidDataException($"A frame should have a valid integer as number, you provided an empty value. Location: {lastCallMsg}");

                    // We can set the number of the frame, but we can not yet
                    // set the type. So all are initiated with the Unassigned type.
                    if (int.TryParse(x, out var nr))
                    {
                        Frames.Add(new Frame { Number = nr, Type = "Unassigned" });
                        lastCall = nr;
                    }
                    else
                    {
                        throw new InvalidDataException($"A frame should have a valid integer as number, you provided an invalid value. Location: {lastCallMsg}");
                    }
                });

                // lets process our data row by row, we start with our first row
                // at B4, so first row 5 starting from column B.

                for (var i = 5 + searchResult.RowOffset; i <= maxRows; i++)
                {
                    // B = 2 for our column
                    var currentColumn = 1;

                    TryGetCellValue(out var service, currentColumn++.GetExcelName() + i, worksheet);
                    TryGetCellValue(out var tension, currentColumn++.GetExcelName() + i, worksheet);
                    TryGetCellValue(out var cableType, currentColumn++.GetExcelName() + i, worksheet);
                    TryGetCellValue(out var cableTrackFromTo, currentColumn++.GetExcelName() + i, worksheet);
                    TryGetCellValue(out var cableNr, currentColumn++.GetExcelName() + i, worksheet);
                    TryGetCellValue(out var lineNumber, currentColumn++.GetExcelName() + i, worksheet);
                    TryGetCellValue(out var remarks, currentColumn++.GetExcelName() + i, worksheet);
                    TryGetCellValue(out var edx, currentColumn++.GetExcelName() + i, worksheet);
                    TryGetCellValue(out var depth, currentColumn++.GetExcelName() + i, worksheet);
                    TryGetCellValue(out var length, currentColumn++.GetExcelName() + i, worksheet);
                    TryGetCellValue(out var temp1, currentColumn++.GetExcelName() + i, worksheet);
                    TryGetCellValue(out var temp2, currentColumn++.GetExcelName() + i, worksheet);
                    TryGetCellValue(out var temp3, currentColumn++.GetExcelName() + i, worksheet);

                    // check for empty lines, and skip them
                    var key = Cable.GetKeyName(service, cableType, cableTrackFromTo, cableNr);
                    if (string.IsNullOrWhiteSpace(key))
                        continue;

                    // What frames is the current cable a member of? While we
                    // are here, lets also set the type of the frame
                    var currentCableFrames = allCells["N" + i + ":" + maxColumns.GetExcelName() + i].GetListOfFirstRow();

                    var resultCable = new Cable(cableTrackFromTo, new Service(service), cableType, cableNr)
                    {
                        Frames = new List<Guid>(),
                        Tension = tension,
                        CableNr = cableNr,
                        LineNumber = lineNumber,
                        Remarks = remarks,
                        EDX = edx,
                        Length = length,
                        Depth = depth,
                        Temp1 = temp1,
                        Temp2=temp2,
                        Temp3 = temp3
                    };

                    // The array currentCableFrames should have same index as
                    // our frames list so if we iterate over it and it contains
                    // type-text, we know what frame to apply to current object,
                    // and we can set its frame-type property
                    for (var y = 0; y < currentCableFrames.Count; y++)
                    {
                        var currentType = currentCableFrames[y];

                        if (string.IsNullOrWhiteSpace(currentType))
                            continue;

                        // Set type of current frame
                        Frames[y].Type = currentType;

                        // Add frame GUID to cable's list of Frames
                        resultCable.Frames.Add(Frames[y].Id);
                    }
                    if (!CableSet.Exists(x => x.KeyName.Equals(key)))
                        CableSet.Add(resultCable);
                    else throw new InvalidDataException($"When reading matrix, a non unique cable was found, cables are unique if the combination of [service][cable-type][from/to] is unique! Row:{i}, key:\"{key}\"");
                }
            }

            return true;
        }

        public override void Write()
        {
            // This is the header we add to each matrix frame
            string[] header = { "Service", "Tension", "Cable Type", "Cabeltrack from/to", "Cable nr.", "Line nr.", "Remarks", "EDX", "Depth (cm)", "Length (m)", "Temp1", "Temp2", "Temp3", "Placement" };
            string[] smallColumns = { "G","I", "J", "K", "L", "M" };

            // Add our sheet to new Excel, if it already exist we append it.
            using (var package = new ExcelPackage(File))
            {
                var worksheet = package.Workbook.Worksheets.Add("XlsxAgent.01");

                var headerStyle = package.Workbook.Styles.CreateNamedStyle("headerStyle");
                headerStyle.Style.Fill.PatternType = ExcelFillStyle.Solid;
                headerStyle.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightCyan);
                headerStyle.Style.Font.Bold = true;
                headerStyle.Style.Font.Size = 8;

                var smallStyle = package.Workbook.Styles.CreateNamedStyle("smallStyle");
                smallStyle.Style.Font.Bold = false;
                smallStyle.Style.ShrinkToFit = false;
                smallStyle.Style.Font.Size = 8;

                var smallHeaderStyle = package.Workbook.Styles.CreateNamedStyle("smallHeaderStyle");
                smallHeaderStyle.Style.Font.Bold = false;
                smallHeaderStyle.Style.ShrinkToFit = false;
                smallHeaderStyle.Style.Font.Size = 8;
                smallHeaderStyle.Style.Fill.PatternType = ExcelFillStyle.Solid;
                smallHeaderStyle.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);

                var smallHeaderStyleShrink = package.Workbook.Styles.CreateNamedStyle("smallHeaderStyleShrink");
                smallHeaderStyleShrink.Style.Font.Bold = true;
                smallHeaderStyleShrink.Style.ShrinkToFit = true;
                smallHeaderStyleShrink.Style.Font.Size = 8;
                smallHeaderStyleShrink.Style.Fill.PatternType = ExcelFillStyle.Solid;
                smallHeaderStyleShrink.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);

                // Group cables by service, and sort them alphabetically
                var groupedCables = SortCables(CableSet);

                // Sort frames
                var sortedFrames = Frames.OrderBy(x => x.Number).ToList();

                // we start at the top of page left corner. +1
                var row = 3;

                var column = 1;

                //Create Header
                for (var i = 0; i < header.Length; i++)
                {
                    worksheet.Cells[row, column].StyleName = "headerStyle";
                    worksheet.Cells[row, column++].Value = header[i];
                }

                // First row set 3
                row++;

                // add matrix numbers for frames
                for (var i = 0; i < sortedFrames.Count; i++)
                {
                    var range = ConstructRange(header, row, i);
                    var formula = ConstructFormula(GetExcelAddress(i + header.Count(), row - 3), range);

                    worksheet.Cells[row - 3, i + header.Count()].Value = sortedFrames[i].Type;
                    worksheet.Cells[row - 3, i + header.Count()].StyleName = "smallHeaderStyle";
                    worksheet.Cells[row - 2, i + header.Count()].Formula = formula;
                    worksheet.Cells[row - 2, i + header.Count()].StyleName = "smallHeaderStyleShrink";
                    worksheet.Cells[row, i + header.Count()].StyleName = "headerStyle";
                    worksheet.Cells[row, i + header.Count()].Value = sortedFrames[i].Number;
                }

                // Add empty rows and columns

                worksheet.InsertColumn(worksheet.Dimension.Columns + 1, ExtraColumns, 12);

                // add styles and formula to empty lines
                for (var i = sortedFrames.Count; i < worksheet.Dimension.Columns - header.Count(); i++)
                {
                    var range = ConstructRange(header, row, i);
                    var formula = ConstructFormula(GetExcelAddress(i + header.Count(), row - 3), range);

                    worksheet.Cells[row - 3, i + header.Count()].StyleName = "smallHeaderStyle";
                    worksheet.Cells[row - 2, i + header.Count()].Formula = formula;
                    worksheet.Cells[row - 2, i + header.Count()].StyleName = "smallHeaderStyleShrink";
                    worksheet.Cells[row, i + header.Count()].StyleName = "headerStyle";
                }

                // reset columns
                column = 1;

                // set row 4
                row++;

                foreach (var group in groupedCables)
                    foreach (var cable in group)
                    {
                        // Create cable and matrix info

                        var cableCells = new string[header.Length + sortedFrames.Count - 1];
                        cableCells[0] = cable.Service.Name;
                        cableCells[1] = cable.Tension;
                        cableCells[2] = cable.CableType;
                        cableCells[3] = cable.CableTrackFromTo;
                        cableCells[4] = cable.CableNr;
                        cableCells[5] = cable.LineNumber;
                        cableCells[6] = cable.Remarks;
                        cableCells[7] = cable.EDX;
                        cableCells[8] = cable.Depth;
                        cableCells[9] = cable.Length;
                        cableCells[10] = cable.Temp1;
                        cableCells[11] = cable.Temp2;
                        cableCells[12] = cable.Temp3;

                        // from cell 10 to +Frame count we need to write cable-type
                        for (var i = 0; i < sortedFrames.Count; i++)
                        {
                            var frameToFind = sortedFrames[i];
                            if (cable.Frames.Contains(frameToFind.Id))
                            {
                                cableCells[i + 13] = sortedFrames[i].Type;
                            }
                            else
                                cableCells[i + 13] = "";
                        }

                        //Create CABLE
                        for (var i = 0; i < cableCells.Length; i++)
                        {
                            if (i == 0)
                                worksheet.Cells[row, column].StyleName = "headerStyle";
                            else if (i > 9)
                                worksheet.Cells[row, column].StyleName = "smallStyle";

                            worksheet.Cells[row, column++].Value = string.IsNullOrWhiteSpace(cableCells[i]) ? null : cableCells[i];
                        }

                        // reset columns
                        column = 1;

                        // Up row
                        row++;
                    }

                worksheet.InsertRow(worksheet.Dimension.Rows + 1, ExtraRows, 5);

                worksheet.DefaultColWidth = .1;
                worksheet.Cells.AutoFitColumns();

                // Style small columns with actual content in them
                foreach (var columnAddress in smallColumns)
                {
                    var nr = columnAddress.GetExcelColumnNumber();
                    worksheet.Column(nr).Width = 3;
                    // EDIT: BJORN CATTOOR:: see issue #7 (https://bitbucket.org/cadsmurfs/xlsxagent/issues/7/fix-issue-in-headerstyle)
                    // overflow behavior is an wanted behavior.
                    worksheet.Column(nr).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                }
                var maxCol = worksheet.Dimension.Columns;

                // set matrix box sizes
                var startCol = "N".GetExcelColumnNumber();
                for (var i = startCol; i <= maxCol; i++)
                {
                    worksheet.Column(i).Width = 4;
                }

                // limit column width to 65
                for (var i = 1; i < maxCol; i++)
                {
                    if (worksheet.Column(i).Width > 65)
                        worksheet.Column(i).Width = 65;
                }

                // Freeze the titles
                worksheet.View.FreezePanes(Column: "N".GetExcelColumnNumber(), Row: 5);

                // set zoom factor to a comfortable level
                worksheet.View.ZoomScale = 85;

                #region HIGHLIGHT SELECTED ROWS

                // Add VBA to project
                /*
                 Private Sub Worksheet_SelectionChange(ByVal Target As Range)
                 If Application.CutCopyMode = False Then
                 Application.Calculate
                 End If
                 End Sub
                 */
                var code = "Private Sub Worksheet_SelectionChange(ByVal Target As Range)" + Environment.NewLine;
                code += "If Application.CutCopyMode = False Then" + Environment.NewLine;
                code += "Application.Calculate" + Environment.NewLine;
                code += "End If" + Environment.NewLine;
                code += "End Sub";
                package.Workbook.CreateVBAProject();
                var candidate = package.Workbook.VbaProject.Modules[1];
                candidate.Code = code;

                var expression = worksheet.ConditionalFormatting.AddExpression(new ExcelAddress("A5:" + worksheet.Dimension.Columns.GetExcelName() + worksheet.Dimension.Rows));
                expression.Formula = "=OR(CELL(\"col\")=COLUMN(),CELL(\"row\")=ROW())";
                expression.Style.Fill.PatternType = ExcelFillStyle.Solid;
                expression.Style.Fill.BackgroundColor.Color = System.Drawing.Color.PapayaWhip;
                expression.Style.Font.Bold = true;
                expression.Style.Font.Color.Color = System.Drawing.Color.Blue;

                #endregion

                package.Save();
            }
        }

        #endregion

        #region Private Methods

        private static string ConstructFormula(string currentFrameCellName, string range)
        {
            //? Replace sting interpolation by string.Concat for better performance
            //- dutch: =TEKST.SAMENVOEGEN("p: ";AANTAL.ALS(E5:E11;"Type 2"))
            //- English: =CONCATENATE(\"{sortedFrames[i].Type}: \",COUNTIF({range},\"{sortedFrames[i].Type}\"))"

            // return "=CONCATENATE(\"" + currentFrame.Type + ": \",COUNTIF(" +
            // range + ",\"" + currentFrame.Type + "\"))";
            return "=COUNTIF(" + range + "," + currentFrameCellName + ")";
        }

        private static string GetExcelAddress(int column, int row)
        {
            return column.GetExcelName() + row;
        }

        private string ConstructRange(string[] header, int row, int i)
        {
            //? Replace sting interpolation by string.Concat for better performance
            //? $"{(i + header.Count()).GetExcelName()}{row + 1}:{(i + header.Count()).GetExcelName()}{row + CableSet.Count}"
            var rowLetter = (i + header.Count()).GetExcelName();

            return rowLetter + (row + 1) + ":" + rowLetter + row + CableSet.Count;
        }

        #endregion
    }
}