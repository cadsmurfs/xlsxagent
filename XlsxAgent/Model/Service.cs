﻿using System;

namespace XlsxAgent.Model
{
    public class Service
    {
        public Service(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException(nameof(name));

            if (!AllowedServices.AllNames.Contains(name))
                throw new ArgumentOutOfRangeException($"A cable should always have a service known by the application. Service name -->({name})");
            Name = name;
            SortWeight = AllowedServices.AllNames.IndexOf(name);
        }

        public int SortWeight { get; private set; }
        public string Name { get; private set; }
    }
}