﻿using System;
using System.Collections.Generic;

namespace XlsxAgent.Model
{
    public class Cable
    {
        #region Private Fields

        private string _keyName;

        #endregion

        #region Public Constructors

        public Cable(string cableTrackFromTo, Service service, string cableType, string cableNr)
        {
            if (string.IsNullOrWhiteSpace(cableTrackFromTo))
            {
                throw new ArgumentException("A cable should always have a name.", nameof(cableTrackFromTo));
            }

            if (string.IsNullOrWhiteSpace(cableType))
            {
                throw new ArgumentException("A cable should always have a cableType assigned.", nameof(cableType));
            }

            Service = service ?? throw new ArgumentException($"A cable should always have a service assigned. Object -->({cableTrackFromTo})", nameof(service));
            CableTrackFromTo = cableTrackFromTo;
            CableType = cableType;
            Service = service;
            CableNr= cableNr;
        }

        #endregion Public Constructors

        #region Public Properties

        public string CableTrackFromTo { get; }
        public string CableType { get; }

        /// <summary>
        /// This array is a list of Id's of frames where the cable is featured.
        /// </summary>
        public IList<Guid> Frames { get; set; }

        public string CableNr { get; set; }
        /// <summary>
        /// This property is used to identify uniqueness of an cable object.
        /// </summary>
        public string KeyName => _keyName ?? (_keyName = GetKeyName(Service.Name, CableType, CableTrackFromTo, CableNr));
        public string LineNumber { get; set; }
        public string Remarks { get; set; }
        public Service Service { get; }
        public string Tension { get; set; }
        public string EDX { get; set; }
        public string Depth { get; set; }
        public string Length { get; set; }
        public string Temp1 { get; set; }
        public string Temp2 { get; set; }
        public string Temp3 { get; set; }

        #endregion

        #region Public Methods

        public static string GetKeyName(string service, string cableType, string cableTrackFromTo,string cableNr)
        {
            return $"{service}{cableType}{cableTrackFromTo}{cableNr}";
        }

        #endregion
    }
}