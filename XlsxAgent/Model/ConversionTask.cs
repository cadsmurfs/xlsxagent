﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XlsxAgent.Data.ReaderWriters;
using XlsxAgent.Extensions;

namespace XlsxAgent.Model
{
    /// <summary>
    /// This class holds our application model objects, and application state.
    /// </summary>
    public class ConversionTask
    {
        #region Public Fields

        /// <summary>
        /// This property holds debug information about the conversion process.
        /// </summary>
        public readonly StringBuilder DebugLog = new StringBuilder();

        #endregion

        #region Public Constructors

        public ConversionTask(string inputFile, string outputFile, bool debug = false, bool openFile = false, bool noRecycleBin = true)
        {
            InputFile = inputFile; OutputFile = outputFile; Debug = debug; OpenFile = openFile; NoRecycleBin = noRecycleBin;

            if (string.IsNullOrWhiteSpace(InputFile))
                throw new NullReferenceException();
            if (!string.IsNullOrWhiteSpace(outputFile))
                OutputFileInfo = new FileInfo(OutputFile);
            else OutputFile = null;
            InputFileInfo = new FileInfo(InputFile);
            WDebug("├───" + inputFile + "]");
        }

        #endregion

        #region Internal Enums

        internal enum ReaderType
        {
            InfrabelMatrix,
            TucTable
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// This is the string holding the error information of the appModel.
        /// </summary>
        public string Error { get; private set; }

        /// <summary>
        /// This is true when conversion was a success.
        /// </summary>
        public bool Failed { get; private set; } = true;

        /// <summary>
        /// This is the path to the input-file.
        /// </summary>
        public string InputFile { get; }

        /// <summary>
        /// This is the path set for the output-file.
        /// </summary>
        public string OutputFile { get; private set; }

        #endregion

        #region Private Properties

        private static bool Debug { get; set; }
        private static bool OpenFile { get; set; }
        private FileInfo InputFileInfo { get; }
        private bool NoRecycleBin { get; set; }
        private FileInfo OutputFileInfo { get; }

        #endregion

        #region Public Methods

        /// <summary>
        /// </summary>
        /// <param name="infrabelMatrixSuffix">  
        /// Suffix string used to create name.
        /// </param>
        /// <param name="overwriteExistingFiles">
        /// True when overwrite is wanted.
        /// </param>
        /// <param name="direction">             
        /// MtT [matrix to TUC] or TtM [TUC to matrix]
        /// </param>
        /// <returns> Path to the resulting file. Returns null when failed. </returns>
        public string ConvertFile(string infrabelMatrixSuffix, bool overwriteExistingFiles, string direction)
        {
            if (direction == null)
                throw new ArgumentNullException("When converting file(s), a direction should always be provided!");

            System.Diagnostics.Debug.Assert(direction == "MtT" || direction == "TtM");

            if (!InputFileInfo.Exists)
            {
                Error = "The input-file could not be found.";
                WDebug("Conversion has been canceled.");
                Failed = true;
                return null;
            }

            if (direction == "MtT")
            {
                var infraReader = new InfrabelReaderWriter
                {
                    File = InputFileInfo
                };

                WDebug($"│ Trying to read as infrabel- MATRIX. [{infraReader.File.FullName}]");

                if (infraReader.Read())
                {
                    WDebug($"└──success: Document containing {infraReader.Frames.Count} frames, and {infraReader.CableSet.Count} cables.");

                    var tucWriter = new TUCReaderWriter
                    {
                        File = (OutputFile == null) ? new FileInfo(CreateNewFileName(infraReader.File.FullName, infrabelMatrixSuffix, ReaderType.TucTable)) : OutputFileInfo,
                        CableSet = infraReader.CableSet,
                        Frames = infraReader.Frames
                    };
                    if (ExecuteBaseWrite(overwriteExistingFiles, tucWriter)) return tucWriter.File.FullName;
                    else return null;
                }
            }

            if (direction == "TtM")
            {
                var tucReader = new TUCReaderWriter
                {
                    File = InputFileInfo
                };
                WDebug($"│ Trying to read as TUC-tabel document. [{tucReader.File.FullName}]");

                if (tucReader.Read())
                {
                    WDebug($"├──success: Document containing {tucReader.Frames.Count} frames, and {tucReader.CableSet.Count} cables.");

                    var infraWriter = new InfrabelReaderWriter
                    {
                        File = (OutputFile == null) ? new FileInfo(CreateNewFileName(tucReader.File.FullName, infrabelMatrixSuffix, ReaderType.InfrabelMatrix)) : OutputFileInfo,
                        CableSet = tucReader.CableSet,
                        Frames = tucReader.Frames,
                        ExtraColumns = Properties.Settings.Default.MatrixWriteExtraColumns,
                        ExtraRows = Properties.Settings.Default.MatrixWriteExtraRows
                    };

                    if (ExecuteBaseWrite(overwriteExistingFiles, infraWriter)) return infraWriter.File.FullName;
                    else return null;
                }
            }

            Failed = true;
            Error = $"└──failed: file[{InputFile}] could not be converted. MODE=[{direction}]";
            return null;
        }

        #endregion

        #region Internal Methods

        internal static IList<ConversionTask> ProcessFolder(ProgramCommandLineSettings settings, Action<string> console, string infrabelMatrixSuffix)
        {
            var inputfiles = GetFilesFromFolder(
                settings.InputPath,
                settings.Direction.Equals("MtT", StringComparison.CurrentCultureIgnoreCase),
                infrabelMatrixSuffix,
                settings.IncludeSubfolders);

            var sw = new Stopwatch();
            sw.Start();
            if (inputfiles != null && inputfiles.Count() > 0)
            {
                console($"Converting {inputfiles.Count()} file(s)...");
                var unknownErrors = new List<string>();
                var expectedErrors = new List<string>();
                var result = new System.Collections.Concurrent.ConcurrentBag<ConversionTask>();
                Parallel.ForEach(inputfiles, (currentFile) =>
                       {
                           try
                           {
                               var t = new ConversionTask(currentFile, null, settings.Debug, settings.OpenFile, settings.NoRecycleBin);
                               t.ConvertFile(infrabelMatrixSuffix, settings.Override, settings.Direction);
                               result.Add(t);
                           }
                           catch (InvalidDataException ex)
                           {
                               expectedErrors.Add($"Data error =>[{Path.GetFileName(currentFile)}] ───> \"{ex.Message}\"");
                           }
                           catch (ArgumentOutOfRangeException ex)
                           {
                               expectedErrors.Add($"Argument OutOfRange =>[{Path.GetFileName(currentFile)}] ───> \"{ex.Message.Replace("\r\n", "  ")}\"");
                           }
                           catch (ArgumentException ex)
                           {
                               expectedErrors.Add($"Argument exeption =>[{Path.GetFileName(currentFile)}] ───> \"{ex.Message.Replace("\r\n", "  ")}\"");
                           }
                           catch (Exception ex)
                           {
                               unknownErrors.Add($"Unexpected error =>[{Path.GetFileName(currentFile)}] ───> \"{ex.Message.Replace("\r\n", "  ")}\"");
                           }
                       });

                // Report Progress
                if (unknownErrors.Count > 0)
                    console($"We processed {inputfiles.Count()} file(s), {unknownErrors.Count} file(s) could not be read. {result.Count(x => x.Failed)} failed to be processed.");
                else
                    console($"We succesfully converted {result.Count()}/{inputfiles.Count()} file(s).");
                sw.Stop();
                console($"Execution time {sw.Elapsed.TotalSeconds} seconds.\r\n");

                // Report on not expected exceptions.
                if (unknownErrors.Count > 0)
                {
                    console($"├──List of {unknownErrors.Count} unexpected error(s)");
                    // Unexpected exceptions
                    foreach (var error in unknownErrors)
                        console("├──>" + error);
                    console("└──|end\r\n");
                }
                if (expectedErrors.Count > 0)
                {
                    // Known ERRORS errors
                    console($"├──Listing {expectedErrors.Count} known error(s)");
                    foreach (var error in expectedErrors)
                        console("├──>" + error);
                    console("└──|end\r\n");
                }

                return result.ToList();
            }

            return null;
        }

        #endregion

        #region Private Methods

        private static string CreateNewFileName(string originalFullName, string matrixSuffix, ReaderType type)
        {
            var filename = Path.GetFileNameWithoutExtension(originalFullName);
            var left = Path.GetDirectoryName(originalFullName);
            var ext = ".xlsx";
            if (type == ReaderType.InfrabelMatrix)
                ext = ".xlsm";

            if (filename.Length == 0)
                throw new ArgumentException("Filename too short, or invalid", nameof(originalFullName));
            if (type == ReaderType.InfrabelMatrix)
            {
                if (filename.EndsWith("." + matrixSuffix, StringComparison.CurrentCultureIgnoreCase))
                {
                    return originalFullName;
                }
                else
                    return left + @"\" + filename + "." + matrixSuffix + ext;
            }

            if (type == ReaderType.TucTable)
            {
                if (filename.EndsWith(matrixSuffix, StringComparison.CurrentCultureIgnoreCase))
                {
                    {
                        // When the file is only one char in length, it means
                        // the filename is made up out of our matrixSuffix. So
                        // we return the original path.
                        if (filename.Length == 1) return originalFullName;
                        return left + @"\" + filename.Substring(0, filename.Length - matrixSuffix.Length - 1) + ext;
                    }
                }
                else
                    return left + @"\" + filename + ext;
            }

            throw new InvalidOperationException();
        }

        /// <summary>
        /// This function scans for all xlsx files within a given folder.
        /// </summary>
        /// <param name="folderPath">           Folder path </param>
        /// <param name="getMatrixFiles">      
        /// Set this to true if you only want files with names that end with the
        /// provided infrabelMatrixSuffix.
        /// </param>
        /// <param name="infrabelMatrixSuffix">
        /// The search parameter for the suffix.
        /// </param>
        /// <param name="includeSubFolder">    
        /// Set this to true if you want to include sub-folders, if set false we
        /// search the top directory only.
        /// </param>
        /// <returns>
        /// IEnumerable{string} containing paths of files where conditions were met.
        /// </returns>
        private static IEnumerable<string> GetFilesFromFolder(string folderPath, bool getMatrixFiles, string infrabelMatrixSuffix, bool includeSubFolder = true)
        {
            //-- Get files from working folder
            var files = Directory.GetFiles(folderPath, "*", includeSubFolder ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);

            var xlsxFiles = files.Where(filePath =>
            {
                var ext = Path.GetExtension(filePath);
                var testEx = ".xlsx";
                if (getMatrixFiles) testEx = ".xlsm";
                if (ext.Equals(testEx, StringComparison.CurrentCultureIgnoreCase))
                {
                    var fileName = Path.GetFileNameWithoutExtension(filePath);
                    if (getMatrixFiles)

                    {
                        if (fileName.EndsWith(infrabelMatrixSuffix))
                            return true;
                        return false;
                    }
                    else
                    {
                        if (fileName.EndsWith(infrabelMatrixSuffix))
                            return false;
                        return true;
                    }
                }
                return false;
            });

            return xlsxFiles;
        }

        private bool ExecuteBaseWrite(bool overwriteExistingFiles, BaseReaderWriter writer)
        {
            if (overwriteExistingFiles)
                MoveFileToRecycleBin(writer, NoRecycleBin);

            if (!File.Exists(writer.File.FullName))
                writer.Write();
            else
            {
                WDebug($"FAILED to create file at --> [{writer.File.FullName}]");
                Error = $"└──[{InputFile}] could NOT be converted. The output file already exists.";
                Failed = true;
                return false;
            }
            OutputFile = writer.File.FullName;

            WDebug($"└──NEW File created at: [{writer.File.FullName}]");

            if (OpenFile)
            {
                HandleOpeningOfFile(writer);
            }

            Error = ""; Failed = false; return true;
        }

        private void HandleOpeningOfFile(BaseReaderWriter rw)
        {
            WDebug($"   └─>Opening file.");
            var proc = Process.Start(rw.File.FullName);
            proc.WaitForInputIdle(1000);
        }

        private void MoveFileToRecycleBin(BaseReaderWriter rw, bool NoRecycleBin = false)
        {
            if (rw.File.Exists)
            {
                if (NoRecycleBin)
                {
                    WDebug($"├─>Permanently deleting the already existing file:[{rw.File.FullName}]");
                    rw.File.Delete();
                }
                else
                {
                    WDebug($"├─>Deleting [to the Recycle Bin] the already existing file:[{rw.File.FullName}]");
                    Microsoft.VisualBasic.FileIO.FileSystem.DeleteFile(rw.File.FullName,
                    Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs,
                    Microsoft.VisualBasic.FileIO.RecycleOption.SendToRecycleBin);
                }
            }
        }

        private void WDebug(string msg)
        {
            if (Debug) DebugLog.AppendT(msg);
        }

        #endregion
    }
}