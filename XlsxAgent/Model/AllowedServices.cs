﻿using System.Linq;
using System.Collections.Generic;

namespace XlsxAgent.Model
{
    public static class AllowedServices
    {
        public static List<string> AllNames = Program.AllowedServices.Split(";".ToCharArray()).ToList();
    }
}