﻿using System;

namespace XlsxAgent.Model
{
    public class Frame
    {
        #region Public Constructors

        public Frame()
        {
            Id = Guid.NewGuid();
        }

        #endregion Public Constructors

        #region Public Properties

        public Guid Id { get; }
        public int Number { get; set; }
        public string Type { get; set; }

        #endregion Public Properties
    }
}