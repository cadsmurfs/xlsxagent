$loc = Get-Location
$AppPath = Split-Path -Parent $loc.Path

# Create our alias if it does not yet exist
if(!(Test-Path alias:xagent)) { 
    new-alias xagent "$AppPath\XlsxAgent.exe"
}

xagent -i $loc --override --convert mtt --NoRecycleBin --includeSub --debug