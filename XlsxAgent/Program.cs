﻿using CommandLineParser.Arguments;
using CommandLineParser.Validation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using XlsxAgent.Model;

namespace XlsxAgent
{
    /// <summary>
    /// This application is developed to transfer a TUC excel format to an
    /// Infrabel format.
    /// </summary>
    public class Program
    {
        private const string InfrabelMatrixSuffix = "M";

        public static string AllowedServices => Properties.Settings.Default.AllowedServices;

        #region Private Methods

        /// <summary>
        /// Main
        /// </summary>
        /// <param name="args"> </param>
        private static void Main(string[] args)
        {
            var parser = new CommandLineParser.CommandLineParser();
            var settings = new ProgramCommandLineSettings();
            parser.ExtractArgumentAttributes(settings);

            try
            {
                parser.ParseCommandLine(args);
            }
            catch (Exception ex)
            {
                WrtMsg($"Command line arguments failed to parse: {ex.Message}");
            }
            try
            {
                if (!parser.ParsingSucceeded)
                {
                    parser.ShowUsage();
                    Console.ReadKey();
                    return;
                }

                if (settings.Help)
                { parser.ShowUsage(); return; }

                if (settings.ShowArguments)
                    parser.ShowParsedArguments();

                // Check what the user has provided us with, is it a file or a
                // folder does it exist, etc ... get the file attributes for
                // file or directory
                bool isFolder = File.GetAttributes(settings.InputPath).HasFlag(FileAttributes.Directory);

                if (isFolder)
                {
                    // Override ettings.OpenFile when processing folders,
                    // opening tons of files will hang windows.
                    settings.OpenFile = false;
                    if (settings.Direction == null)
                    { WrtMsg("You should provide the Convert argument when you are processing a folder."); parser.ShowUsage(); return; }

                    WrtMsg($"INFO: Running in folder modus. Direction: {settings.Direction}" + Environment.NewLine);

                    var results = ConversionTask.ProcessFolder(settings, WrtMsg, InfrabelMatrixSuffix);

                    if (results != null)
                        (results as List<ConversionTask>).ForEach(result =>
                        {
                            if (settings.Debug)
                            {
                                Console.Write(result.DebugLog.ToString());
                            }
                            if (result.Failed)
                            {
                                WrtMsg(result.Error);
                            }
                            if (settings.Debug || result.Failed)
                                Console.WriteLine();
                        });
                    else WrtMsg("We found no files to process.");
                }
                else
                {
                    if (settings.Direction != null)
                    { WrtMsg("You should NOT provide the Convert argument when you are only processing one file at a time."); parser.ShowUsage(); return; }

                    ConversionTask TtMTask = null, MtTTask = null;

                    WrtMsg($"INFO: Running in file modus. This means we will try to convert in both directions." + Environment.NewLine);

                    WrtMsg($"├───  Table to Matrix  ────");
                    try
                    {
                        TtMTask = new ConversionTask(settings.InputPath, settings.OutputFile, settings.Debug, settings.OpenFile, settings.NoRecycleBin);
                        TtMTask.ConvertFile(InfrabelMatrixSuffix, settings.Override, "TtM");
                    }
                    catch (InvalidDataException ex) { WrtMsg($"├──>Invalid data: \"{ex.Message}\""); }
                    catch (ArgumentOutOfRangeException ex) { WrtMsg($"├──>Argument out of Range: \"{ex.Message.Replace("\r\n", "  ")}\""); }
                    catch (ArgumentException ex) { WrtMsg($"├──>Argument Exception: \"{ex.Message.Replace("\r\n", "  ")}\""); }
                    catch (Exception ex) { WrtMsg($"├──>Unknown, unexpected exception: \"{ex.Message.Replace("\r\n", "  ")}\""); }
                    if (settings.Debug && TtMTask.DebugLog != null) Console.Write(TtMTask.DebugLog.ToString());
                    if (TtMTask.Error != null) WrtMsg(TtMTask.Error);
                    if (TtMTask != null) { WrtMsg(TtMTask.Failed ? "└──X─ Convertion failed." : "└──V─OK!"); }

                    if (TtMTask != null && TtMTask.Failed)
                    {
                        Console.WriteLine();
                        WrtMsg($"├───  Matrix to Table  ────");
                        try
                        {
                            MtTTask = new ConversionTask(settings.InputPath, settings.OutputFile, settings.Debug, settings.OpenFile, settings.NoRecycleBin);
                            MtTTask.ConvertFile(InfrabelMatrixSuffix, settings.Override, "MtT");
                        }
                        catch (InvalidDataException ex) { WrtMsg($"├──>Invalid data: \"{ex.Message}\""); }
                        catch (ArgumentOutOfRangeException ex) { WrtMsg($"├──>Argument out of Range: \"{ex.Message.Replace("\r\n", "  ")}\""); }
                        catch (ArgumentException ex) { WrtMsg($"├──>Argument Exception: \"{ex.Message.Replace("\r\n", "  ")}\""); }
                        catch (Exception ex) { WrtMsg($"├──>Unknown, unexpected exception: \"{ex.Message.Replace("\r\n", "  ")}\""); }
                    }

                    if (MtTTask != null && settings.Debug && MtTTask.DebugLog != null) Console.Write((MtTTask.DebugLog.ToString()));
                    if (MtTTask != null && MtTTask.Error != null && MtTTask.Failed) WrtMsg(MtTTask.Error);
                    else if (MtTTask != null) { WrtMsg(MtTTask.Failed ? "└──X─ Convertion failed." : "└──V─OK!"); }
                }
            }
            catch (Exception ex)
            {
                if (parser.ParsingSucceeded)
                    WrtMsg($" └─ Oops! Wa have some problems parsing your commandline arguments: [{Path.GetFileName(settings.InputPath)}] ==> \"{ex.Message}\"");
                else
                    WrtMsg($" └─ Ai ai, an unexpected exception occured: \"{ex.Message}\"");
            }
        }

        private static void WrtMsg(string message)
        {
            Console.WriteLine($"{DateTime.Now:T}:  {message}");
        }

        #endregion Private Methods
    }

    // arguments j and k can not be used together with arguments l or m
    //[DistinctGroupsCertification("e,o", "p,r")]
    internal class ProgramCommandLineSettings
    {
        // https://github.com/j-maly/CommandLineParser summary of currently
        // available short codes d, a, c, r, u, h, e, i, o,s

        #region Public Properties

        [SwitchArgument('d', "debug", false, Description = "Set whether to show debug information or not. When omitted equals FALSE.")]
        public bool Debug { get; set; }

        [SwitchArgument('a', "arguments", false, Description = "Set whether to show debug information about arguments parsed on command-line. When omitted equals FALSE.")]
        public bool ShowArguments { get; set; }

        [EnumeratedValueArgument(typeof(string), 'c', LongName = "convert", AllowedValues = "MtT;TtM", Example = "-c MtT -c TtM",
            FullDescription = "When we are processing files found in the provided folder we need to know the intended direction. MtT = Matrix to Table  TtM = Table to Matrix. You should not provide this when only converting a single file. Single files get converted by guessing.",
            Description = "This is the direction for converting.", IgnoreCase = true, Optional = true, AllowMultiple = false)]
        public string Direction { get; set; }

        [SwitchArgument('r', "override", false, Description = "This argument is used to let the application know that it is allowed to override existing files.")]
        public bool Override { get; set; }

        [SwitchArgument('u', "NoRecycleBin", false, Description = "If this is set we will NOT use the recycle bin to delete files.")]
        public bool NoRecycleBin { get; set; }

        [SwitchArgument('h', "help", false, Description = "See command-line arguments help. When omitted equals FALSE.")]
        public bool Help { get; set; }

        [SwitchArgument('e', "excel", false, Description = "When this is set, after conversion the file will be opened with the default program associated with it.\r\n\t\tThis is only used when processing a single file!")]
        public bool OpenFile { get; set; }

        [ValueArgument(typeof(string), 'i', "input", Description = "Set the path to the input file (or folder when using a folder) you would like to use.", Optional = false)]
        public string InputPath { get; set; }

        [ValueArgument(typeof(string), 'o', "output", Description = "Set the path to the output file.", Optional = true)]
        public string OutputFile { get; set; }

        // includeSubfolderSearching
        [SwitchArgument('s', "includeSub", false, Description = "When this is set, we will include all sub-folders when a folder has been provided.")]
        public bool IncludeSubfolders { get; set; }

        #endregion Public Properties
    }
}