# XlsxAgent

## Description
This application is developed to transform Excel files from one custom-format to another custom-format by creating custom readers. Current implementation converts
from the TUC-Rail table model, to the Infrabel matrix model, and vica versa.

## Current Version

- [V12 (click to download)](https://bitbucket.org/cadsmurfs/xlsxagent/downloads/V12.zip)
- After downloading unpack zip file to a location of your choosing.

## Adding executable to the windowst PATH
Add the installation folder to you [environment path](https://superuser.com/questions/284342/what-are-path-and-other-environment-variables-and-how-can-i-set-or-use-them) variable in windows, if you do not want to type in the complete path to the executable.

## Further automation
Further automation can be achieved by writing shell scripts using the script languages available in windows.
An example is included in the `PowerShell` folder.

## Usage from Powershell folder

- In the unpacked project folder u can find the PowerShell folder.
- In that folder you will find 2 files. (and 2 debug scripts)
- Copy all your excel files you would like to convert into this folder, or any sub-folder.
- Run the `run.matrix-to-table.bat`, or the `run.table-to-matrix.bat` cmd script
    - The default CLI arguments used are: `--override --convert [direction] --NoRecycleBin --includeSub`
- You should see all files being processed.
- Processing of batch files is now executed in parallel threads.

Feel free to change PowerShell script if you want to add your own filtering.

### Known issues

1. By company group policy it is possible **you need to unblock the PowerShell script** file.
   - How to do that u can find here:
     1. Browse to the ZIP file you downloaded using Windows Explorer
     2. Right-click the ZIP file and select Properties.
     3. On the General tab, under Security, click the Unblock check-box.
     4. Click OK.   
     5. You can now unzip the content to the desired location.
2. When a file is open for editing, the file can not be overridden.

## Naming of files
The application uses the original filename as output filename, if no output filename has been specified.

For matrix-like file's an .M is added to the filename.
Matrix files have as extension ".xlsm" (this is needed because the template contains some VBA code), table files have as extension ".xlsx"

### Using the "-o" argument

The -o argument is use to override the output filename.
Whenever u are using the `-o <<filename.ext>>` CLI argument, you are responsible for providing the right extension. 

__The application will not automatically add an extension to the provided filename when u are setting the output filename yourself!__

### Example of auto naming

> Auto naming is used when **NO** -o argument is provided!

Providing a matrix `filename.M.xlsm` will become `filename.xlsx` when converted to a table.
Providing a table `filename.xlsx` will become `filename.M.xlsm` when converted to a matrix.

## Deleted files

All deleted files are moved to the recycle bin of your computer, unless you add the -u CLI argument,
then we **permanently** delete the file.

> Careful this will override existing files if the -r command-line argument has been set.

## Usage from command-line

### Example convert one file

In the below example we feed the application one file, no convert direction is specified.
We use the `-r` argument to let the application know we want it to override any file already existing.
Be careful though, this will also override the input file itself.

For more options please see the Usage section below.

`xlsxAgent.exe -i "c:\test\excelfile.xlsx" -r`

### Example of converting files in a folder

In the example below we process all files that have a filename (convert="TtM") **NOT ENDING on ".M"**.
We search all sub-folders.
We override any existing files.
We do not use recycle-bin

`XlsAgent.exe -i c:\temp\ --override --convert ttm --NoRecycleBin --includeSub`

### Usage:

```cmd
Usage:
        -d, --debug[optional]... Set whether to show debug information or not. When omitted equals FALSE.

        -a, --arguments[optional]... Set whether to show debug information about arguments parsed on command-line. When omitted equals FALSE.

        -c, --convert[optional]... This is the direction for converting.
Example: -c MtT -c TtM

When we are processing files found in the provided folder we need to know the intended direction. MtT = Matrix to Table  TtM = Table to Matrix. You should not provide this when only converting a single file. Single files get converted by guessing.

        -r, --override[optional]... This argument is used to let the application know that it is allowed to override existing files.

        -u, --NoRecycleBin[optional]... If this is set we will NOT use the recycle bin to delete files.

        -h, --help[optional]... See command-line arguments help. When omitted equals FALSE.

        -e, --excel[optional]... When this is set, after conversion the file will be opened with the default program associated with it.
                This is only used when processing a single file!

        -i, --input... Set the path to the input file (or folder when using a folder) you would like to use.

        -o, --output[optional]... Set the path to the output file.

        -s, --includeSub[optional]... When this is set, we will include all sub-folders when a folder has been provided.
```
