﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XlsxAgent.Model;

namespace XlsxAgent.Test.DataGeneration
{
    internal static class Generator
    {
        #region Private Fields

        internal static Random random = new Random(DateTime.Now.Millisecond);

        #endregion Private Fields

        #region Public Methods

        public static List<Cable> GenerateRandomCables(List<Frame> frames, int amount)
        {
            // Get service strings
            var services = Program.AllowedServices;
            var allowedServices = services.Split(";".ToCharArray());

            if (frames.Count < 5)
                throw new ArgumentOutOfRangeException(nameof(frames));

            var cables = new List<Cable>();

            for (int i = 0; i < amount; i++)
            {
                // make sure to generate a unique CableKey
                var candidateName = GenerateRandomString(random.Next(1, 200));
                var cableType = GenerateRandomString(random.Next(1, 10));
                var cableNr = GenerateRandomString(random.Next(1, 2)) + random.Next(1, 999);
                var service = new Service(allowedServices[random.Next(1, allowedServices.Count())]);
                bool isNotUnique = cables.Find(x => x.KeyName.Equals(Cable.GetKeyName(service.Name, cableType, candidateName, cableNr), StringComparison.CurrentCulture)) != null;
                while (isNotUnique)
                {
                    candidateName = GenerateRandomString(random.Next(5, 20));
                    cableType = GenerateRandomString(random.Next(1, 10));
                    isNotUnique = cables.Find(x => x.KeyName.Equals(Cable.GetKeyName(service.Name, cableType, candidateName, cableNr), StringComparison.CurrentCulture)) != null;
                }

                cables.Add(new Cable(candidateName, service, cableType, cableNr)
                {
                    CableNr = GenerateRandomString(random.Next(2, 10)),
                    Remarks = GenerateRandomString(random.Next(0, 25)),
                    Tension = GenerateRandomString(random.Next(2, 3)),
                    LineNumber = GenerateRandomString(random.Next(2, 4)),
                    Frames = new List<Guid>(),
                    EDX = GenerateRandomString(random.Next(0, random.Next(1, 400))),
                    Temp1 = GenerateRandomString(random.Next(0, random.Next(1, 400))),
                    Temp2 = GenerateRandomString(random.Next(0, random.Next(1, 400))),
                    Temp3 = GenerateRandomString(random.Next(0, random.Next(1, 400))),
                    Length = $"{random.NextDouble() * random.Next(1, 400)}",
                    Depth = $"{random.NextDouble() * random.Next(1, 400)}"
                });
            }

            // Now add random frames to the cables
            for (int i = 0; i < cables.Count; i++)
            {
                var dice = random.Next(1, (frames.Count / 2) + 1);
                // add to 0-5 frames
                for (int j = 0; j <= dice; j++)
                {
                    var luckyFrame = frames[random.Next(0, frames.Count)];
                    if (!cables[i].Frames.Contains(luckyFrame.Id))
                        cables[i].Frames.Add(luckyFrame.Id);
                }
            }

            return cables;
        }

        public static List<Frame> GenerateRandomFrames(int amount)
        {
            var r = new List<Frame>();
            for (int i = 0; i < amount; i++)
            {
                var n = random.Next(0, 4);
                var t = "";
                switch (n)
                {
                    case 0:
                        t = "Type 1";
                        break;

                    case 1:
                        t = "Type 2";
                        break;

                    case 2:
                        t = "Type 3";
                        break;

                    case 3:
                        t = "Type 4";
                        break;

                    default:
                        t = "Unassigned";
                        break;
                }
                r.Add(new Frame { Number = i, Type = t });
            }

            return r;
        }

        public static string GenerateRandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        #endregion Public Methods
    }
}