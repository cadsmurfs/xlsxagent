﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using XlsxAgent.Data.ReaderWriters;
using XlsxAgent.Model;
using XlsxAgent.Test.DataGeneration;

namespace XlsxAgent.Test
{
    [TestClass]
    public class ReadWriteTest
    {
        #region Private Fields

        private const string basePath = @"C:\Users\cwn8400\Documents\Source\Repos\xlsxagent\XlsxAgent.Test\Data\";
        private const string ReadInfrabelUnassignedMatrix = basePath + @"InfrabelReaderWriter.xlsx";
        private const string MatrixNoFrameIdString = basePath + @"cab-0660-007.550-010.273-Zedelgem_no_frame_id.M.xlsm";

        //private const string InfrabelReaderXlsxFilePathUnassigned = @"InfrabelReaderWriter - unassigned in read.xlsx";
        private const string TucReadEmptyFrame = basePath + @"TucReaderWriter - Inlezen met een Leeg Frame.xlsx";

        private const string TucReaderWriterXlsxFilePath = basePath + @"TucReaderWriter.xlsx";
        private const string TucReaderEmptyLines = basePath + @"empty lines and unknown frame-types.xlsx";

        //private const string ReaderTucNameCableNotProvided = @"empty lines and unknown frame-types.xlsx";
        private const string ReadInfrabelUnknownService = basePath + @"InfrabelReaderWriter unknown service.xlsx";

        private const string ReadTucUnknownService = basePath + @"TucReader unknown service.xlsx";
        private const string ReadInfrabelOriginal = basePath + @"Testfile.NoEmptyLines.M.xlsm";
        private const string ReadInfrabelHasEmptyLines = basePath + @"Testfile.EmptyLines.M.xlsm";
        private const string Matrix_MalformedHeader = basePath + @"Matrix_MalformedHeader.xlsx";
        private const string TUCTabel_MalformedHeader = basePath + @"TUCTabel_MalformedHeader.xlsx";
        private const string ReadTucSkipUnknownLines = basePath + @"TucReaderSkipUnknownLines.xlsx";
        private const string ReadTucSkipUnknownLinesControl = basePath + @"TucReaderSkipUnknownLines_Control.xlsx";
        private const string RealTest1 = basePath + @"Real Test 1.xlsx";
        private const string RealTest2 = basePath + @"Real Test 2.xlsx";
        private const string RealTest3 = basePath + @"Real Test 3.xlsx";
        private const string TestUnknowLinesMatrixContol = basePath + @"bk_0.xlsx";
        private const string TestUnknowLinesMatrix = basePath + @"bk_0.M.xlsm";
        private const string ExceptionNoIntInMatrixHeader = basePath + @"ExceptionNoIntInMatrixHeader.Infrabel.Matrix.xlsx";
        private const string CablesWithSameNames = basePath + @"n2cab-0503-115.000-110.200-Oostende.M.xlsm";
        private const string SimularNameEndingAsSuffix = basePath + @"cab-0660-007.550-010.273-Zedelgem.xlsx";
        private const string SimularNameEndingAsSuffixExpectedResult = basePath + @"cab-0660-007.550-010.273-Zedelgem.M.xlsx";

        #endregion Private Fields

        #region Public Methods

        [TestMethod]
        public void HandleFilenameEndingsSimularToSuffix()
        {
            var model = new ConversionTask(inputFile: SimularNameEndingAsSuffix, outputFile: null, debug: false, openFile: false, noRecycleBin: true);

            model.ConvertFile(infrabelMatrixSuffix: "M", overwriteExistingFiles: true, direction: "TtM");

            // Check if output-file name matches expected result
            Assert.AreNotEqual(model.OutputFile, SimularNameEndingAsSuffixExpectedResult);
        }

        [TestMethod]
        public void ReadInfrabelCablesWithSameNames()
        {
            var model = new ConversionTask(CablesWithSameNames, null, false, false, true);

            // Read from written infrabel style
            var matrixReader = new InfrabelReaderWriter()
            {
                File = new System.IO.FileInfo(CablesWithSameNames)
            };

            matrixReader.Read();

            // Convert the matrix-file to a table
            var result = model.ConvertFile("M", true, "MtT");

            var tableReader = new TUCReaderWriter()
            {
                File = new System.IO.FileInfo(result)
            };

            // read the table
            tableReader.Read();

            // Now they should be the same
            CompareDataReaders(matrixReader, tableReader);
        }

        [TestMethod]
        public void ReadInfrabelEmptyLinesTest()
        {
            // Read from written infrabel style
            var reader = new InfrabelReaderWriter()
            {
                File = new System.IO.FileInfo(ReadInfrabelHasEmptyLines)
            };

            // Read from written infrabel style
            var readerOriginal = new InfrabelReaderWriter()
            {
                File = new System.IO.FileInfo(ReadInfrabelOriginal)
            };

            reader.Read();
            readerOriginal.Read();

            // Now they should be the same
            CompareDataReaders(readerOriginal, reader);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidDataException))]
        public void ReadMatrixMalformedHeader()
        {
            // Read from written infrabel style
            var reader = new InfrabelReaderWriter()
            {
                File = new System.IO.FileInfo(Matrix_MalformedHeader)
            };
            reader.Read();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidDataException))]
        public void ReadTUCTabelMalformedHeader()
        {
            // Read from written infrabel style
            var reader = new TUCReaderWriter()
            {
                File = new System.IO.FileInfo(TUCTabel_MalformedHeader)
            };
            reader.Read();
        }

        [TestMethod]
        public void RealTest1Test()
        {
            ConversionTask model = new ConversionTask(RealTest1, null, false, false, true);

            // Read the model
            model.ConvertFile("M", true, "TtM");

            // Check if output-file has been created
            Assert.IsTrue(System.IO.File.Exists(model.OutputFile));
        }

        [TestMethod]
        public void RealTest2Test()
        {
            ConversionTask model = new ConversionTask(RealTest2, null, false, false, true);

            // Read the model
            model.ConvertFile("M", true, "TtM");

            // Check if output-file has been created
            Assert.IsTrue(System.IO.File.Exists(model.OutputFile));
        }

        [TestMethod]
        public void RealTest3Test()
        {
            // Here we test the reading of on unknown format
            ConversionTask model = new ConversionTask(RealTest3, null, false, false, true);

            // Read the model
            model.ConvertFile("M", true, "TtM");

            // Check if output-file has been created
            Assert.IsTrue(System.IO.File.Exists(model.OutputFile));
        }

        [TestMethod]
        public void ReadTucSkipUnknownLinesTest()
        {
            // Read from written infrabel style
            var reader = new TUCReaderWriter()
            {
                File = new System.IO.FileInfo(ReadTucSkipUnknownLines)
            };

            // Read from written infrabel style
            var readerOriginal = new TUCReaderWriter()
            {
                File = new System.IO.FileInfo(ReadTucSkipUnknownLinesControl)
            };

            reader.Read();
            readerOriginal.Read();

            // Now they should be the same
            CompareDataReaders(readerOriginal, reader);
        }

        [TestMethod]
        public void TestUnknowLinesMatrixContolTest()
        {
            // Read from written infrabel style
            var reader = new InfrabelReaderWriter()
            {
                File = new System.IO.FileInfo(TestUnknowLinesMatrix)
            };

            // Read from written TUC style
            var readerOriginal = new TUCReaderWriter()
            {
                File = new System.IO.FileInfo(TestUnknowLinesMatrixContol)
            };

            reader.Read();
            readerOriginal.Read();

            // Now they should be the same
            CompareDataReaders(readerOriginal, reader);
        }

        [TestMethod]
        public void ReadInfrabelUnassignedMatrixTest()
        {
            // Read from written infrabel style
            var reader = new InfrabelReaderWriter()
            {
                File = new System.IO.FileInfo(ReadInfrabelUnassignedMatrix)
            };

            reader.Read();

            foreach (var frame in reader.Frames)
            {
                Assert.IsFalse(frame.Type == "Unassigned", "all frames should be set.");
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CableNameNullTest()
        {
            _ = new Cable("", new Service(Program.AllowedServices.Split(";".ToCharArray())[0]), "type","");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CableTypeNullTest()
        {
            _ = new Cable("name", new Service(Program.AllowedServices.Split(";".ToCharArray())[0]), "","");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidDataException))]
        public void ExceptionNoIntInMatrixHeaderTest()
        {
            // Read from written infrabel style
            var reader = new InfrabelReaderWriter()
            {
                File = new System.IO.FileInfo(ExceptionNoIntInMatrixHeader)
            };

            reader.Read();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CableServiceNullTest()
        {
            _ = new Cable("Name", null, "type","");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void CableServiceNotMemberTest()
        {
            // Read from written infrabel style
            _ = new Cable("Name", new Service("Not a member."), "type","");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void InfrabelReaderCableServiceNotMemberTest()
        {
            // Read from written infrabel style
            var reader = new InfrabelReaderWriter()
            {
                File = new System.IO.FileInfo(ReadInfrabelUnknownService)
            };

            reader.Read();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidDataException))]
        public void MatrixNoFrameId()
        {
            // Read from written infrabel style
            var reader = new InfrabelReaderWriter()
            {
                File = new System.IO.FileInfo(MatrixNoFrameIdString)
            };

            reader.Read();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TucReaderCableServiceNotMemberTest()
        {
            // Read from written infrabel style
            var reader = new TUCReaderWriter()
            {
                File = new System.IO.FileInfo(ReadTucUnknownService)
            };

            reader.Read();
        }

        [TestMethod]
        public void ReadTucReaderWriterWithEmptyFrame()
        {
            // Read from written infrabel style
            var reader = new TUCReaderWriter()
            {
                File = new System.IO.FileInfo(TucReadEmptyFrame)
            };

            reader.Read();

            // We should have a frame that has not been assigned to any cable
            // (number 3)
            var caseObject = reader.Frames.Find(el => el.Number == 17);
            Assert.IsFalse(caseObject == null, "Frame with number 3 not found in test-case.");
            Assert.IsFalse(caseObject.Type != "Unassigned", "Frame-type, if not set should be unassigned.");

            // No cable should exist that is a member of this caseObject
            var cableMembers = reader.CableSet.FindAll(el => el.Frames.Contains(caseObject.Id)).ToList();
            Assert.IsTrue(cableMembers.Count == 0, "No cable should exist that is a member of this caseObject.");
        }

        [TestMethod]
        public void ReadTucWithEmptyLines()
        {
            // Read from written infrabel style
            var reader = new TUCReaderWriter()
            {
                File = new System.IO.FileInfo(TucReaderEmptyLines)
            };

            reader.Read();

            // We should have a frame with the number 411

            Assert.IsTrue(reader.Frames.Find(x => x.Number == 411) != null, "We should have a frame with the number 411");
        }

        [TestMethod]
        public void WriteAndReadInfrabelStyleEquality()
        {
            // Generate Frames
            List<Frame> Frames = Generator.GenerateRandomFrames(200);

            // Create some test data
            List<Cable> Cables = Generator.GenerateRandomCables(Frames, 500);

            // Write to infrabel style
            var writer = new InfrabelReaderWriter()
            {
                CableSet = Cables,
                Frames = Frames,
                File = new System.IO.FileInfo(ReadInfrabelUnassignedMatrix)
            };

            if (writer.File.Exists)
            {
                writer.File.Delete();
            }

            writer.Write();
            // Read from written infrabel style
            var reader = new InfrabelReaderWriter()
            {
                File = new System.IO.FileInfo(ReadInfrabelUnassignedMatrix)
            };

            reader.Read();

            // Now they should be the same
            CompareDataReaders(writer, reader);
        }

        [TestMethod]
        public void WriteAndReadTucStyleEquality()
        {
            // Generate Frames
            List<Frame> Frames = Generator.GenerateRandomFrames(200);

            // Create some test data
            List<Cable> Cables = Generator.GenerateRandomCables(Frames, 500);

            // Write to style
            var writer = new TUCReaderWriter()
            {
                CableSet = Cables,
                Frames = Frames,
                File = new System.IO.FileInfo(TucReaderWriterXlsxFilePath)
            };

            if (writer.File.Exists)
            {
                writer.File.Delete();
            }

            writer.Write();
            // Read from written style
            var reader = new TUCReaderWriter()
            {
                File = new System.IO.FileInfo(TucReaderWriterXlsxFilePath)
            };

            reader.Read();

            // Now they should be the same
            CompareDataReaders(writer, reader);
        }

        [TestMethod]
        public void BulkTest()
        {
            int testCount = 25;
            int internalFrames = Generator.random.Next(20, 500);
            int internalCables = Generator.random.Next(10, 150);
            string testpath = @"c:\temp\xlsxagent\bulk_tests\bk_";
            List<List<Frame>> generatedFrames = new List<List<Frame>>();
            List<List<Cable>> generatedCables = new List<List<Cable>>();
            // Generate models
            for (int i = 0; i < testCount; i++)
            {
                // Generate Frames
                generatedFrames.Add(Generator.GenerateRandomFrames(internalFrames));

                // Create some test data
                generatedCables.Add(Generator.GenerateRandomCables(generatedFrames.Last(), internalCables));

                // reset random size
                internalFrames = Generator.random.Next(20, 500);
                internalCables = Generator.random.Next(10, 150);
            }

            var testPath = new DirectoryInfo(testpath.TrimEnd("bk_".ToCharArray()));
            // if directory does not exist create it.
            if (!testPath.Exists)
            {
                testPath.Create();
            }

            System.Threading.Tasks.Parallel.For(0, testCount, (i) =>
            //for (int i = 0; i < testCount; i++)
            {
                // Write to style
                var wTucTableToFS_Ds1 = new TUCReaderWriter()
                {
                    CableSet = generatedCables[i],
                    Frames = generatedFrames[i],
                    File = new System.IO.FileInfo(testpath + i + ".xlsx")
                };

                var wInfraMatrixToFS_Ds2 = new InfrabelReaderWriter()
                {
                    CableSet = generatedCables[i],
                    Frames = generatedFrames[i],
                    File = new System.IO.FileInfo(testpath + i + ".M.xlsm")
                };

                // Now the generated, in memory data should be the same.
                CompareDataReaders(wTucTableToFS_Ds1, wInfraMatrixToFS_Ds2);

                if (wTucTableToFS_Ds1.File.Exists)
                {
                    wTucTableToFS_Ds1.File.Delete();
                }

                if (wInfraMatrixToFS_Ds2.File.Exists)
                {
                    wInfraMatrixToFS_Ds2.File.Delete();
                }

                wTucTableToFS_Ds1.Write();
                wInfraMatrixToFS_Ds2.Write();

                // Read from written style
                var rTucTableFromFS_Ds1 = new TUCReaderWriter()
                {
                    File = new System.IO.FileInfo(testpath + i + ".xlsx")
                };

                var rInfraMatrixFromFS_Ds2 = new InfrabelReaderWriter()
                {
                    File = new System.IO.FileInfo(testpath + i + ".M.xlsm")
                };

                rTucTableFromFS_Ds1.Read();
                rInfraMatrixFromFS_Ds2.Read();

                // The data generated should be the same as the date read from
                // the filesystem
                CompareDataReaders(rInfraMatrixFromFS_Ds2, rTucTableFromFS_Ds1);
                CompareDataReaders(rTucTableFromFS_Ds1, rInfraMatrixFromFS_Ds2);
            });
        }

        #endregion Public Methods

        #region Private Methods

        private void CompareDataReaders(BaseReaderWriter x, BaseReaderWriter y)
        {
            var extraFileString = "  Files: " + x.File.Name + "; " + y.File.Name;

            // Lists of frames and cables should have the same amount of items
            Assert.IsTrue(x.CableSet.Count == y.CableSet.Count, "Lists of cables should have the same amount of items." + extraFileString);
            Assert.IsTrue(x.Frames.Count == y.Frames.Count, "Lists of frames should have the same amount of items." + extraFileString);

            // all frames should be equal
            foreach (var frame in x.Frames)
            {
                // each frame has unique number value so we should only find one
                var candidates = y.Frames.FindAll(item => item.Number == frame.Number).ToList();
                Assert.IsFalse(candidates.Count == 0, "A frame could not be found with same number." + extraFileString);
                Assert.IsTrue(candidates.Count == 1, "Too many frames could be found with same number." + extraFileString);

                // now compare type, it should be same
                Assert.AreEqual(frame.Type, candidates.First().Type, $"Frame types do not compare [frame number:{frame.Number}]: \"{frame.Type}\"; \"{candidates.First().Type}\"" + extraFileString);
            }

            // all cables should be equal
            foreach (var xCable in x.CableSet)
            {
                // each cable has unique keyName string value so we should only
                // find one of them
                var candidatesx = y.CableSet.FindAll(item => item.KeyName == xCable.KeyName).ToList();
                Assert.IsFalse(candidatesx.Count == 0, "A cable could not be found with same keyName." + extraFileString);
                Assert.IsTrue(candidatesx.Count == 1, "Too many cables found with same keyName." + extraFileString);
                var candidatesy = y.CableSet.FindAll(item => item.KeyName == xCable.KeyName).ToList();
                Assert.IsFalse(candidatesy.Count == 0, "A cable could not be found with same keyName." + extraFileString);
                Assert.IsTrue(candidatesy.Count == 1, "Too many cable could be found with same keyName." + extraFileString);

                // now compare type, it should be same
                Cable yCable = candidatesy.First();
                Assert.AreEqual(xCable.KeyName, yCable.KeyName, false, "CableTrackFromTo names are not equal." + extraFileString);
                Assert.AreEqual(xCable.Service.Name, yCable.Service.Name, false, "Service names are not equal." + extraFileString);
                Assert.AreEqual(xCable.CableType, yCable.CableType, false, "CableType names are not equal." + extraFileString);
                Assert.AreEqual(xCable.CableNr, yCable.CableNr, false, "Cable nrs are not equal." + extraFileString);
                Assert.AreEqual(xCable.LineNumber, yCable.LineNumber, false, "LineNumbers are not equal." + extraFileString);
                Assert.AreEqual(xCable.Tension, yCable.Tension, false, "Tension is not equal." + extraFileString);
                Assert.AreEqual(xCable.Remarks, yCable.Remarks, false, "Remarks are not equal." + extraFileString);
                Assert.AreEqual(xCable.EDX, yCable.EDX, false, "EDX names are not equal." + extraFileString);
                Assert.AreEqual(xCable.Depth, yCable.Depth, false, "Depth is not equal." + extraFileString);
                Assert.AreEqual(xCable.Length, yCable.Length, false, "Length is not equal." + extraFileString);
                Assert.AreEqual(xCable.Temp1, yCable.Temp1, false, "Temp1 is not equal." + extraFileString);
                Assert.AreEqual(xCable.Temp2, yCable.Temp2, false, "Temp2 is not equal." + extraFileString);
                Assert.AreEqual(xCable.Temp3, yCable.Temp3, false, "Temp3 is not equal." + extraFileString);

                // Number of appointed frames should be the same
                Assert.IsTrue(xCable.Frames.Count.Equals(yCable.Frames.Count), "Both cables should have the same amount of frames assigned." + extraFileString);

                // Same frame numbers should be in each cable collection of frames
                foreach (var xGuid in xCable.Frames)
                {
                    var xFrameNumber = x.Frames.Find(j => j.Id.Equals(xGuid)).Number;
                    // Go get actual frames from our yCable
                    var actualYFrames = y.Frames.FindAll(p => yCable.Frames.Contains(p.Id)).ToList();

                    Assert.IsTrue(actualYFrames.Find(g => g.Number.Equals(xFrameNumber)) != null, "Same frame numbers should be in each cable collection of frames" + extraFileString);
                }
            }
        }

        #endregion Private Methods
    }
}